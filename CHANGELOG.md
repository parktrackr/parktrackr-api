# Changelog

Key changes to new versions

## 0.1.0

* 0.1.0 Initial implementation of Efteling and Europa-Park.
* 0.1.1 Implementation of Prettier and a Changelog.
* 0.1.2 Added tests for Efteling, removed placeholder test and some hotfixing.
* 0.1.3 Implemented dependency injection and added one Europa-Park test.
* 0.1.4 Implemented connection functions and reworked Entity, Park, ConfigBase classes to abstract classes.
* 0.1.5 Reworked interfaces of APIs, added Toverland, reworked connections function factory.
* 0.1.6 Added Walibi parks.
* 0.1.7 Added MWS and mocks for Efteling.
* 0.1.8 Added Phantasialand and Hellendoorn.
* 0.1.9 Cleanup, added more structure to project, error codes added, latlongs fixed of some parks, added Park methods for validation.
* 0.1.10 Made basic Attraction POI model, with some validation, regarding combined id and parkId.
* 0.1.11 Attraction schema and resolvers added.
* 0.1.12 Temp Model added, for temporary data, like tokens.
* 0.1.13 Added Efteling process for Attraction POI and Translation.
* 0.1.14 Added Toverland process for Attraction POI and Translation, removed env variable in handlers.
* 0.1.15 Added Europa-Park process for Attraction POI and Translation, cleaned up some mocks.
* 0.1.15a Fixed Mongo Error
* 0.1.15b Added gitlab env variable
* 0.1.15c Hotfix beforeEach mocha
* 0.1.15d Hotfix Mongo usage in EuropaPark
* 0.1.15e Added Mock Mongoose
* 0.1.15f Expanded timeout time mocha
* 0.1.15g Changed the mocking of mongoose
* 0.1.15h Changed mocking function
* 0.1.15i Removed Mock Mongoose, added statements in EuropePark
* 0.1.16 Added Phantasialand process for Attraction POI and Translation.
* 0.1.17 Added Avonturenpark Hellendoorn process for Attraction POI and Translation.
* 0.1.18 Added Walibi process for Attraction POI and Translation.
* 0.1.19 Added slugify function & comments to functions.
* 0.1.20 Made unit tests possible without env file.
* 0.1.21 Added loadAttractionsPOI for Efteling and EuropaPark.
* 0.1.22 Added loadAttractionPOIS to every park, reworked data flow in Park classes, disabled tests for now.