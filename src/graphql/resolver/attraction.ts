import { Attraction, AttractionModel, Translation } from '../../mongoose/model';
import api from '../../lib';
import * as connections from '../../lib/parks/connections';
import { ProcessedPOI } from '../../lib/parks/park.types';

// const park = new api.parks.Efteling(connections.Efteling());

export default {
  Query: {
    attractions: (
      parent: undefined,
      { offset = 0, limit = 100, filter = null }: { offset: number; limit: number; filter: string | null },
    ): Promise<Attraction[]> => {
      if (filter === null) {
        return AttractionModel.find({}).skip(offset).limit(limit);
      } else {
        return AttractionModel.find({
          originalName: { $regex: filter, $options: 'i' },
        });
      }
    },
    attraction: (parent: undefined, { id }: { id: string }): Promise<Attraction> => {
      if (!id) {
        throw new Error('No id was provided.');
      }
      return AttractionModel.findOne({ id });
    },
  },
  Mutation: {
    loadAttractionPOIS: async (parent: undefined, { id }: { id: string }): Promise<ProcessedPOI[]> => {
      const { parks } = api;
      const {
        Efteling,
        EuropaPark,
        Hellendoorn,
        Phantasialand,
        Toverland,
        WalibiBelgium,
        WalibiHolland,
        WalibiRhoneAlpes,
      } = parks;

      if (id === 'efteling') {
        const park = new Efteling(connections.Efteling());
        const data = await park.loadAttractionPOIS();
        return AttractionModel.insertMany(data);
      } else if (id === 'europa-park') {
        const park = new EuropaPark(connections.EuropaPark());
        const data = await park.loadAttractionPOIS();
        return AttractionModel.insertMany(data);
      } else if (id === 'toverland') {
        const park = new Toverland(connections.Toverland());
        const data = await park.loadAttractionPOIS();
        return AttractionModel.insertMany(data);
      } else if (id === 'phantasialand') {
        const park = new Phantasialand(connections.Phantasialand());
        const data = await park.loadAttractionPOIS();
        return AttractionModel.insertMany(data);
      } else if (id === 'hellendoorn') {
        const park = new Hellendoorn();
        const data = await park.loadAttractionPOIS();
        return AttractionModel.insertMany(data);
      } else if (id === 'walibi-belgium') {
        const park = new WalibiBelgium(connections.WalibiBelgium());
        const data = await park.loadAttractionPOIS();
        return AttractionModel.insertMany(data);
      } else if (id === 'walibi-holland') {
        const park = new WalibiHolland(connections.WalibiHolland());
        const data = await park.loadAttractionPOIS();
        return AttractionModel.insertMany(data);
      } else if (id === 'walibi-rhone-alpes') {
        const park = new WalibiRhoneAlpes(connections.WalibiRhoneAlpes());
        const data = await park.loadAttractionPOIS();
        return AttractionModel.insertMany(data);
      } else {
        return [];
      }
    },
  },
  Attraction: {
    translations: (parent: Attraction, { langs }: { langs: string[] | undefined }): Translation[] => {
      if (langs !== undefined) {
        return parent.translations.filter((translation) => langs.some((lang) => lang === translation.language));
      } else {
        return parent.translations;
      }
    },
  },
};
