import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    attractions(offset: Int, limit: Int, filter: String): [Attraction!]
    attraction(id: String!): Attraction
  }

  extend type Mutation {
    loadAttractionPOIS(id: String!): [Attraction!]
  }

  type Attraction {
    _id: ID!
    id: String!
    originalName: String!
    slug: String!
    parkId: String!
    latitude: Float!
    longitude: Float!
    translations(langs: [String!]): [Translation!]
    createdAt: Date!
    updatedAt: Date!
  }

  type Translation {
    _id: ID!
    language: String!
    name: String!
  }
`;
