import express from 'express';
import mongoose from 'mongoose';
import { ApolloServer } from 'apollo-server-express';
import depthLimit from 'graphql-depth-limit';
import http from 'http';
import cors from 'cors';

import schema from './graphql/schema';
import resolvers from './graphql/resolver';

const app = express();

const uri = 'mongodb://localhost/parktrackr-api';
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
};

mongoose.connect(uri, options);

mongoose.connection.once('open', () => {
  console.log('Connected to: MongoDB');
});

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  introspection: true,
  playground: true,
  validationRules: [depthLimit(5)],
});

app.use('*', cors());

server.applyMiddleware({ app, path: '/graphql' });

const httpServer = http.createServer(app);

httpServer.listen({ port: 3000 }, () => {
  console.log('Apollo Server on http://localhost:3000/graphql');
});
