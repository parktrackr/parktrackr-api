/* eslint-disable @typescript-eslint/no-explicit-any */
import { EventEmitter } from 'events';

const parseConfig = (options: { [key: string]: any; configPrefixes?: string[] } = {}) => {
  const configKeys = Object.keys(options);

  const config: { [key: string]: any } = {};

  options.configPrefixes = ['TPAPI'].concat(options.configPrefixes || []);

  // build this.config object with our settings
  configKeys.forEach((key) => {
    // default prefixes are either "classname_" or "TPAPI"
    //  classes can add more with configPrefixes
    if (options.configPrefixes !== undefined) {
      options.configPrefixes.forEach((prefix: string) => {
        const configEnvName = `${prefix}_${key}`.toUpperCase();

        if (process.env[configEnvName]) {
          // console.log(`Using env variable ${configEnvName}`);
          config[key] = process.env[configEnvName];
          // console.log(` ${key}(env.${configEnvName})=${config[key]}`);
        }
      });
    }

    if (config[key] === undefined) {
      config[key] = options[key];
    } else {
      // convert env variable to number if the base default is a number
      if (typeof config[key] === 'number') {
        config[key] = Number(config[key]);
      } else if (typeof config[key] === 'boolean') {
        // convert any boolean configs too
        config[key] = config[key] === 'true';
      }
    }
  });

  return config;
};

abstract class ConfigBase extends EventEmitter {
  config: { [key: string]: any };

  constructor(options: { configPrefixes?: any[] } = {}) {
    super();

    options.configPrefixes = [this.constructor.name].concat(options.configPrefixes || []);

    this.config = parseConfig(options || {});
  }
}

export default ConfigBase;
