// Efteling
import Efteling from './parks/efteling/efteling';
// Europa-Park
import EuropaPark from './parks/europapark/europapark';
// Attractiepark Hellendoorn
import Hellendoorn from './parks/hellendoorn/hellendoorn';
// Phantasialand
import Phantasialand from './parks/phantasialand/phantasialand';
// Toverland
import Toverland from './parks/toverland/toverland';
// Walibi
import WalibiBelgium from './parks/walibi/walibibelgium';
import WalibiHolland from './parks/walibi/walibiholland';
import WalibiRhoneAlpes from './parks/walibi/walibirhonealpes';

export default {
  parks: {
    Efteling,
    EuropaPark,
    Hellendoorn,
    Phantasialand,
    Toverland,
    WalibiBelgium,
    WalibiHolland,
    WalibiRhoneAlpes,
  },
};
