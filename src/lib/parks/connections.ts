import { ConnectionOptions as EftelingConnectionOptions } from './efteling/efteling.types';
import { ConnectionOptions as EuropaParkConnectionOptions } from './europapark/europapark.types';
import { ConnectionOptions as PhantasialandConnectionOptions } from './phantasialand/phantasialand.types';
import { ConnectionOptions as ToverlandConnectionOptions } from './toverland/toverland.types';
import { ConnectionOptions as WalibiConnectionOptions } from './walibi/walibi.types';

const Efteling = (): EftelingConnectionOptions => {
  const apiKey = process.env.EFTELING_API_KEY || 'efteling_api_key';
  const searchURL = process.env.EFTELING_SEARCH_URL || 'http://parktrackr.nl/mock/efteling/poi/';
  const waitTimesURL = process.env.EFTELING_WAITTIMES_URL || 'http://parktrackr.nl/mock/efteling/waittimes';

  if (!apiKey) throw new Error('No apiKey provided.');
  if (!searchURL) throw new Error('No searchURL provided.');
  if (!waitTimesURL) throw new Error('No waitTimesURL provided.');

  return {
    apiKey,
    searchURL,
    waitTimesURL,
  };
};

const EuropaPark = (): EuropaParkConnectionOptions => {
  const apiURL = process.env.EUROPAPARK_API_URL || 'http://parktrackr.nl/mock/europa-park/poi/';
  const loginURL = process.env.EUROPAPARK_LOGIN_URL || 'http://parktrackr.nl/mock/europa-park/login';
  const refreshURL = process.env.EUROPAPARK_REFRESH_URL || 'http://parktrackr.nl/mock/europa-park/refresh';
  const apiUsername = process.env.EUROPAPARK_USER_NAME || 'europapark_user_name';
  const apiPassword = process.env.EUROPAPARK_PASSWORD || 'europapark_password';

  if (!apiURL) throw new Error('No apiURL provided.');
  if (!loginURL) throw new Error('No loginURL provided.');
  if (!refreshURL) throw new Error('No refreshURL provided.');
  if (!apiUsername) throw new Error('No apiUsername provided.');
  if (!apiPassword) throw new Error('No apiPassword provided.');

  return {
    apiURL,
    loginURL,
    refreshURL,
    apiUsername,
    apiPassword,
  };
};

const Phantasialand = (): PhantasialandConnectionOptions => {
  const apiURL = process.env.PHANTASIALAND_API_URL || 'http://parktrackr.nl/mock/phantasialand/poi/';
  const waitTimesURL =
    process.env.PHANTASIALAND_WAITIMES_URL || 'http://parktrackr.nl/mock/phantasialand/waittimes/LOC';
  const accesToken = process.env.PHANTASIALAND_ACCES_TOKEN || 'acces-token';
  const loc = process.env.PHANTASIALAND_LOC || '0.000,0.000';

  if (!apiURL) throw new Error('No apiAuthKey provided.');
  if (!waitTimesURL) throw new Error('No apiHostURL provided.');
  if (!accesToken) throw new Error('No openingHoursURL provided.');
  if (!loc) throw new Error('No openingHoursURL provided.');

  return {
    apiURL,
    waitTimesURL,
    accesToken,
    loc,
  };
};

const Toverland = (): ToverlandConnectionOptions => {
  const apiAuthKey = process.env.TOVERLAND_AUTH_KEY || 'toverland_auth_key';
  const apiHostURL = process.env.TOVERLAND_API_URL || 'http://parktrackr.nl/mock/toverland/poi';
  const openingHoursURL = process.env.TOVERLAND_OPENINGTIMES_URL || 'http://parktrackr.nl/mock/toverland/openingtimes';

  if (!apiAuthKey) throw new Error('No apiAuthKey provided.');
  if (!apiHostURL) throw new Error('No apiHostURL provided.');
  if (!openingHoursURL) throw new Error('No openingHoursURL provided.');

  return {
    apiAuthKey,
    apiHostURL,
    openingHoursURL,
  };
};

const WalibiHolland = (): WalibiConnectionOptions => {
  const entertainmentURL = process.env.WALIBIHOLLAND_ENTERTAINMENTS_URL || 'http://parktrackr.nl/mock/walibi/poi';

  if (!entertainmentURL) throw new Error('No entertainmentURL provided.');

  return {
    entertainmentURL,
  };
};

const WalibiBelgium = (): WalibiConnectionOptions => {
  const entertainmentURL = process.env.WALIBIBELGIUM_ENTERTAINMENTS_URL || 'http://parktrackr.nl/mock/walibi/poi';

  if (!entertainmentURL) throw new Error('No entertainmentURL provided.');

  return {
    entertainmentURL,
  };
};

const WalibiRhoneAlpes = (): WalibiConnectionOptions => {
  const entertainmentURL = process.env.WALIBIRHONEALPES_ENTERTAINMENTS_URL || 'http://parktrackr.nl/mock/walibi/poi';

  if (!entertainmentURL) throw new Error('No entertainmentURL provided.');

  return {
    entertainmentURL,
  };
};

export { Efteling, EuropaPark, Phantasialand, Toverland, WalibiBelgium, WalibiHolland, WalibiRhoneAlpes };
