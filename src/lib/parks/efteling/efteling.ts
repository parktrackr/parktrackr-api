import fetch from 'node-fetch';
import Park from '../park';
import { ProcessedPOI, Translation } from '../park.types';

// TYPES
import { ConnectionOptions, POI, FetchPOI, POIData } from './efteling.types';

/**
 * Class representing the Efteling park.
 * @extends Park
 */
class Efteling extends Park {
  name: string;
  timezone: string;
  latitude: number;
  longitude: number;
  mainLanguage: string;
  languageOptions: string[];

  /**
   * Create the Efteling park.
   * @param {ConnectionOptions} options - The variables used for the API
   */
  constructor(options: ConnectionOptions) {
    super(options);

    this.name = 'Efteling';
    this.timezone = 'Europe/Amsterdam';

    // Latitude and longitude positioned at the entrance of the park
    this.latitude = 51.65098350641645;
    this.longitude = 5.049916835374731;

    // language options
    this.mainLanguage = 'nl';
    this.languageOptions = ['en', 'fr', 'de', 'nl'];

    const envVariables = [this.config.searchURL, this.config.apiKey, this.config.waitTimesURL];

    this.validateEnvironmentalVariables(envVariables);
  }

  /**
   * Fetch POI
   * @param {string} lang - Language selector
   *
   * @returns {FetchPOI} The data
   */
  private async fetchPOI(lang: string): Promise<FetchPOI> {
    return await fetch(
      this.config.searchURL +
        `search?q.parser=structured&size=1000&q=%28and%20%28phrase%20field%3Dlanguage%20%27$${lang}%27%29%29`,
      {
        method: 'GET',
        headers: {
          Authorization: this.config.apiKey,
        },
      },
    )
      .then((response) => {
        return response.json();
      })
      .then((data: POIData) => {
        return {
          language: lang,
          pois: data.hits.hit,
        };
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  }

  /**
   * Fetch POIS
   *
   * @returns {Promise<FetchPOI[]>} The fetched data
   */
  private async fetchPOIS(): Promise<FetchPOI[]> {
    const promises: Promise<FetchPOI>[] = [];

    this.languageOptions.forEach((lang: string) => {
      promises.push(this.fetchPOI(lang));
    });

    return Promise.all(promises);
  }

  /**
   * Filter POIS
   * @param {FetchPOI[]} arrays - The arrays with data
   * @param {string} filter - the filter string

   *
   * @returns {FetchPOI[]} The filtered FetchPOIS
   */
  private filterPOIS(arrays: FetchPOI[], filter: string): FetchPOI[] {
    return arrays.map((fetchPOI: FetchPOI) => {
      return {
        language: fetchPOI.language,
        pois: fetchPOI.pois.filter((poi: POI) => poi.fields.category === filter),
      };
    });
  }

  /**
   * Process POIS
   * @param {FetchPOI[]} arrays - The arrays with data
   *
   * @returns {ProcessedPOI[]} The processed POIs
   */
  private processPOIS(arrays: FetchPOI[]): ProcessedPOI[] {
    const mainArray = arrays.find((array: FetchPOI) => array.language === this.mainLanguage);

    if (!mainArray) {
      throw new Error();
    }

    return mainArray.pois.map(
      (poi: POI): ProcessedPOI => {
        const { fields } = poi;
        const { id, name, latlon } = fields;
        const [latitude, longitude] = latlon.split(',');

        const translationsObject = this.processTranslations(id, arrays);

        return {
          id,
          originalName: name,
          slug: this.sluggify(name),
          parkId: this.sluggify(this.name),
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          ...(translationsObject ? { translations: translationsObject } : {}),
        };
      },
    );
  }

  /**
   * Process Translations
   * @param {string} id - identifier of the item
   * @param {FetchPOI[]} arrays - The translation arrays
   *
   * @returns {(Translation | undefined)[]} The processed POI Translations
   */
  private processTranslations(id: string, arrays: FetchPOI[]): (Translation | undefined)[] {
    return arrays.map((transArray: FetchPOI) => {
      const item = transArray.pois.find((tr: POI) => tr.fields.id === id);
      if (!item) {
        return;
      }
      const { fields } = item;
      const { name, language } = fields;
      return {
        language,
        name,
      };
    });
  }

  /**
   * Load Attraction POIS
   *
   * @returns {Promise<ProcessedPOI[]>} Attraction POIS
   */
  async loadAttractionPOIS(): Promise<ProcessedPOI[]> {
    const data = await this.fetchPOIS();
    const filtered = this.filterPOIS(data, 'attraction');
    const processed = this.processPOIS(filtered);
    return processed;
  }
}

export default Efteling;
