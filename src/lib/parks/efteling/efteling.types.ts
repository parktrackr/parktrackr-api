interface ConnectionOptions {
  apiKey: string;
  searchURL: string;
  waitTimesURL: string;
}

interface POIData {
  status: Status;
  hits: Hits;
}

interface Status {
  rid: string;
  'time-ms': number;
}
interface Hits {
  found: number;
  start: number;
  hit: POI[];
}

interface POI {
  id: string;
  fields: Fields;
}

interface Fields {
  category: Category;
  hide_in_app: boolean;
  properties?: string[];
  subtitle?: string;
  name: string;
  detail_text?: string;
  text: string;
  can_order: boolean;
  latlon: string;
  targetgroups?: Targetgroup[];
  language: Language;
  image_detailview1: string;
  image: string;
  display_name?: string;
  empire?: Empire;
  showduration: string;
  type?: string;
  id: string;
  has_assortment: boolean;
  alternateid?: string;
  alternatetype?: string;
  alternatelabel?: string;
  image_detailview2?: string;
  image_detailview3?: string;
  image_detailview4?: string;
  name_in_context?: string;
  instruction_text?: string;
  color_codes?: string;
  assortment_image?: string;
  grouped_pois?: string[];
}

enum Category {
  Attraction = 'attraction',
  Domain = 'domain',
  Eventlocation = 'eventlocation',
  FacilitiesGeneric = 'facilities-generic',
  FacilitiesSwimmingpool = 'facilities-swimmingpool',
  FacilitiesToilets = 'facilities-toilets',
  Fairytale = 'fairytale',
  FirstAid = 'first-aid',
  Game = 'game',
  Merchandise = 'merchandise',
  Restaurant = 'restaurant',
  Show = 'show',
}

enum Empire {
  Anderrijk = 'Anderrijk',
  Fantasierijk = 'Fantasierijk',
  Marerijk = 'Marerijk',
  Reizenrijk = 'Reizenrijk',
  Ruigrijk = 'Ruigrijk',
}

enum Language {
  Nl = 'nl',
  DE = 'de',
  FR = 'fr',
  EN = 'en',
}

enum Targetgroup {
  Thrillseekers = 'thrillseekers',
  WholeFamily = 'whole-family',
  YoungestOnes = 'youngest-ones',
}

interface FetchPOI {
  language: string;
  pois: POI[];
}

export { ConnectionOptions, POI, FetchPOI, POIData };
