import ConfigBase from '../configBase';

abstract class Entity extends ConfigBase {
  abstract name: string;
  abstract timezone: string;
  abstract latitude: number;
  abstract longitude: number;
  abstract mainLanguage: string;
  abstract languageOptions: string[];

  constructor(options = {}) {
    super(options);
  }
}

export default Entity;
