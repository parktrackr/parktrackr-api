import fetch from 'node-fetch';
import Park from '../park';
import dotenv from 'dotenv';
dotenv.config();

// TYPES
// import { ProcessedPOI, Translations } from '../park.types';
import { ConnectionOptions, FetchPOI, POI } from './europapark.types';
import { TempModel, Translation } from '../../../mongoose/model';
import { ProcessedPOI } from '../park.types';

const lessThanSixHourAgo = (date: number) => {
  const HOUR = 1000 * 60 * 360;
  const anHourAgo = Date.now() - HOUR;

  return date > anHourAgo;
};

/**
 * Class representing the Europa-Park park.
 * @extends Park
 */
class EuropaPark extends Park {
  name: string;
  timezone: string;
  latitude: number;
  longitude: number;
  mainLanguage: string;
  languageOptions: string[];
  /**
   * Create the Europa-Park park.
   * @param {ConnectionOptions} options
   */
  constructor(options: ConnectionOptions) {
    super(options);

    this.name = 'Europa-Park';
    this.timezone = 'Europe/Berlin';

    // Latitude and longitude positioned at the entrance of the park
    this.latitude = 48.266140769976715;
    this.longitude = 7.722050520358709;

    // language options
    this.mainLanguage = 'de';
    this.languageOptions = ['en', 'fr', 'de', 'nl'];

    const envVariables = [
      this.config.apiURL,
      this.config.loginURL,
      this.config.refreshURL,
      this.config.apiUsername,
      this.config.apiPassword,
    ];

    this.validateEnvironmentalVariables(envVariables);
  }

  /**
   * Fetch Login
   *
   * @return {Promise<POI[] | null>} refreshToken
   */
  private async fetchLogin(): Promise<string | null> {
    return fetch(this.config.loginURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        password: this.config.apiPassword,
        username: this.config.apiUsername,
      }),
    })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        let refresh_token = response.refresh_token;
        refresh_token = { refresh_token: refresh_token };
        return refresh_token;
      })
      .catch((err) => {
        this.throwCustomError(`Fetch Error: ${err.message}`, 'FETCHABORTED');
        return null;
      });
  }

  /**
   * Refresh Token
   *
   * @return {Promise<POI[] | null>} token
   */
  private async fetchJWTToken(): Promise<string | undefined> {
    let existingToken;
    if (process.env.ENVIRONMENT === 'GITLAB' || !process.env.ENVIRONMENT) {
      existingToken = '';
    } else {
      existingToken = await TempModel.findOne({ id: 'EUROPAPARK_JWT_TOKEN' });
    }

    if (existingToken && lessThanSixHourAgo(existingToken.createdAt)) {
      return existingToken.value;
    } else {
      if (process.env.ENVIRONMENT !== 'GITLAB' && process.env.ENVIRONMENT) {
        await TempModel.findOneAndRemove({ id: 'EUROPAPARK_JWT_TOKEN' });
      }
      const refreshToken = await this.fetchLogin();
      if (!refreshToken || refreshToken === null) {
        this.throwCustomError(`Token Error: Refresh Token not provided.`, 'TOKENNOTPROVIDED');
      }
      return fetch(this.config.refreshURL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(refreshToken),
      })
        .then((response) => {
          return response.json();
        })
        .then(async (response) => {
          let token = response.token;
          token = 'Bearer ' + token;
          if (process.env.ENVIRONMENT !== 'GITLAB' && process.env.ENVIRONMENT) {
            const temp = new TempModel({
              id: 'EUROPAPARK_JWT_TOKEN',
              value: token,
            });
            await temp.save();
          }

          return token;
        })
        .catch((err) => {
          this.throwCustomError(`Fetch Error: ${err.message}`, 'FETCHABORTED');
        });
    }
  }

  /**
   * Fetch POI
   * @param {string} lang language selector
   *
   * @return {Promise<{language: string; pois: POI[];} | null>} All Europa-Park POIS from a certain language
   */
  private async fetchPOI(lang: string): Promise<FetchPOI> {
    if (!this.isParameterPresent(lang)) {
      this.throwCustomError('Parameter missing. Parameter not provided: lang.', 'PARAMETERMISSING');
    }

    if (!this.isLanguageAvailable(lang)) {
      if (lang === '') {
        this.throwCustomError(
          `Language is not available. Available languages: ${this.languageOptions}. Language provided: empty string.`,
          'LANGUAGEUNAVAILABLE',
        );
      } else {
        this.throwCustomError(
          `Language is not available. Available languages: ${this.languageOptions}. Language provided: ${lang}.`,
          'LANGUAGEUNAVAILABLE',
        );
      }
    }

    const token = await this.fetchJWTToken();

    if (typeof token !== 'string') {
      throw new Error();
    }

    return fetch(this.config.apiURL + lang, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        JWTAuthorization: token,
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        return {
          language: lang,
          ...response,
        };
      })
      .catch((err) => {
        this.throwCustomError(`Fetch Error: ${err.message}`, 'FETCHABORTED');
      });
  }

  /**
   * Fetch POIS
   *
   * @returns {Promise<POIFetch[] | null>} All Efteling POIS from certain languages
   */
  private async fetchPOIS(): Promise<FetchPOI[]> {
    const promises: Promise<FetchPOI>[] = [];

    this.languageOptions.forEach((lang: string) => {
      promises.push(this.fetchPOI(lang));
    });

    return Promise.all(promises);
  }

  /**
   * Filter POIS
   * @param {FetchPOI[]} arrays - The arrays with data
   * @param {string} filter - the filter string

   *
   * @returns {FetchPOI[]} The filtered FetchPOIS
   */
  private filterPOIS(arrays: FetchPOI[], filter: string): FetchPOI[] {
    return arrays.map((fetchPOI: FetchPOI) => {
      return {
        language: fetchPOI.language,
        pois: fetchPOI.pois.filter((poi: POI) => poi.type === filter),
      };
    });
  }

  /**
   * Process POIS
   * @param {FetchPOI[]} arrays - The arrays with data
   *
   * @returns {ProcessedPOI[]} The processed POIs
   */
  private processPOIS(arrays: FetchPOI[]): ProcessedPOI[] {
    const mainArray = arrays.find((array: FetchPOI) => array.language === this.mainLanguage);

    if (!mainArray) {
      throw new Error();
    }

    return mainArray.pois.map(
      (poi: POI): ProcessedPOI => {
        const { id, name, latitude, longitude } = poi;

        const translationsObject = this.processTranslations(id, arrays);

        return {
          id: `${id}`,
          originalName: name,
          slug: this.sluggify(name),
          parkId: this.sluggify(this.name),
          latitude,
          longitude,
          ...(translationsObject ? { translations: translationsObject } : {}),
        };
      },
    );
  }

  /**
   * Process Translations
   * @param {string} id - identifier of the item
   * @param {FetchPOI[]} arrays - The translation arrays
   *
   * @returns {(Translation | undefined)[]} The processed POI Translations
   */
  private processTranslations(id: number, arrays: FetchPOI[]): (Translation | undefined)[] {
    return arrays.map((transArray: FetchPOI) => {
      const item = transArray.pois.find((tr: POI) => tr.id === id);
      if (!item) {
        return;
      }
      const { name } = item;
      return {
        language: transArray.language,
        name,
      };
    });
  }

  /**
   * Load Attraction POIS
   *
   * @returns {Promise<ProcessedPOI[]>} Attraction POIS
   */
  async loadAttractionPOIS(): Promise<ProcessedPOI[]> {
    const data = await this.fetchPOIS();
    const filtered = this.filterPOIS(data, 'attraction');
    const processed = this.processPOIS(filtered);
    return processed;
  }
}

export default EuropaPark;
