interface ConnectionOptions {
  apiURL: string;
  loginURL: string;
  refreshURL: string;
  apiUsername: string;
  apiPassword: string;
}

interface POI {
  id: number;
  code: number | null;
  type: Type;
  status: Status;
  versions: Version[];
  latitude: number;
  longitude: number;
  name: string;
  descriptionMarkup: string | null;
  description: string;
  excerpt: string | null;
  mainSponsors: [];
  sideSponsors: [];
  poiAttributes: POIAttribute[];
  attributes: Attribute[];
  poiActions: [];
  actions: Action[];
  image: Asset;
  icon: Asset | null;
  iconSvg: Asset | null;
  youtube: Asset | null;
  galleryMedias: Asset[];
  relatedIds: number[];
  openTimes: OpenTime[];
  openTimeDescription: null;
  seasonRelated: boolean | null;
  startAt: Date | null;
  endAt: Date | null;
  validFrom: Date | null;
  validTo: Date | null;
  changedAt: Date;
  minAge: number | null;
  minAgeAdult: number | null;
  maxAge: number;
  minHeight: number | null;
  minHeightAdult: number | null;
  maxHeight: number | null;
  scopes: Scope[];
  areaId: number | null;
  filterIds: number[];
  benefitIds: [] | number[];
  shows: Show[];
  highlight: boolean | null;
  superior: boolean | null;
  stars: number | null;
  queueing: boolean;
  queueingStopped: boolean;
  queueingStoppedAt: null;
  slotCapacity: number;
  slotLength: number;
  slotBuffer: number;
  queueingMaxWaitingTime: number;
  queueingMaxWaitingTimeClosed: number;
  infostele: boolean | null;
  closingCode: null;
  typeCode: number | null;
}

interface Version {
  id: number;
  status: Status;
  name: string;
  descriptionMarkup: string | null;
  description: string;
  excerpt: string | null;
  image: Asset;
  icon: Asset | null;
  iconSvg: IconSVG | null;
  youtube: null;
  galleryMedias: Asset[];
  changedAt: Date;
  startAt: Date | null;
  endAt: Date | null;
  validFrom: Date | null;
  validTo: Date | null;
}

interface Asset {
  id: number;
  name: string;
  autor: Autor | null;
  alt: string | null;
  copyright: null;
  mime: MIME;
  changedAt: Date;
  reference: string;
  provider: string;
  small: string;
  medium: string;
  large: string;
  xlarge: string;
  '480p': string;
  '720p': string;
  '1080p': string;
  '48x48': string;
  '96x96': string;
  '144x144': string;
}

interface POIAttribute {
  id: number;
  ident: Ident;
  icon: Asset | null;
  label: string;
  value: string | null;
  text: string;
}

interface Attribute {
  key: null | string;
  value: null | string;
}

interface Action {
  type: ActionType;
  title: Title;
  action: string;
}

interface IconSVG {
  id: number;
  name: string;
  autor: null;
  alt: null;
  copyright: null;
  mime: MIME;
  changedAt: Date;
  reference: string;
  provider: string;
}

interface OpenTime {
  type: string;
  startAt: Date;
  endAt: Date;
  startAt1: Date | null;
  endAt1: Date | null;
  closed1: boolean;
  startAt2: Date | null;
  endAt2: Date | null;
  closed2: boolean;
  startAt3: Date | null;
  endAt3: Date | null;
  closed3: boolean;
  startAt4: Date | null;
  endAt4: Date | null;
  closed4: boolean;
  startAt5: Date | null;
  endAt5: Date | null;
  closed5: boolean;
  startAt6: Date | null;
  endAt6: Date | null;
  closed6: boolean;
  startAt7: Date | null;
  endAt7: Date | null;
  closed7: boolean;
  changedAt: Date;
}

interface Show {
  id: number;
  status: Status;
  poiId: number;
  name: string;
  descriptionMarkup: string | null;
  description: string;
  excerpt: string | null;
  image: Asset;
  galleryMedias: Asset[];
  characteristics: [] | [][];
  intval: Intval;
  startAt: Date | null;
  endAt: Date | null;
  times: string | null;
  duration: number;
  infostele: boolean;
  inapp: boolean;
  validFrom: Date | null;
  validTo: Date | null;
  changedAt: Date;
  position: number;
}

enum Title {
  Anrufen = 'Anrufen',
  Buchen = 'Buchen',
  EMail = 'E-Mail',
  The360Panorama = '360°-Panorama',
}

enum ActionType {
  Book = 'book',
  Call = 'call',
  Mail = 'mail',
  Panorama = 'panorama',
}

enum Autor {
  EuropaPark = 'Europa-Park',
  MackMedia = 'MackMedia.',
}

enum MIME {
  ImageGIF = 'image/gif',
  ImageJPEG = 'image/jpeg',
  ImagePNG = 'image/png',
  ImageSVGXML = 'image/svg+xml',
  VideoXFlv = 'video/x-flv',
}

enum Ident {
  Acceleration = 'acceleration',
  CapacityBoat = 'capacity_boat',
  CapacityCar = 'capacity_car',
  CapacityGondel = 'capacity_gondel',
  CapacityPerson = 'capacity_person',
  CapacitySeats = 'capacity_seats',
  CapacityTrain = 'capacity_train',
  CoasterElements = 'coaster_elements',
  ConBanquetseating = 'con_banquetseating',
  ConMarquee = 'con_marquee',
  ConMaxperson = 'con_maxperson',
  ConParliamentaryseating = 'con_parliamentaryseating',
  ConRoomsize = 'con_roomsize',
  ConRowsofchairs = 'con_rowsofchairs',
  ConStandingreception = 'con_standingreception',
  ConUshapeseating = 'con_ushapeseating',
  DrivingTime = 'driving_time',
  Height = 'height',
  Length = 'length',
  MaxAcceleration = 'max_acceleration',
  MaxSpeed = 'max_speed',
  Opening = 'opening',
  Producer = 'producer',
  TheoreticalCapacityHour = 'theoretical_capacity_hour',
}

enum Scope {
  Confertainment = 'confertainment',
  Europapark = 'europapark',
  Hotel = 'hotel',
  Resort = 'resort',
  Rulantica = 'rulantica',
  Yullbe = 'yullbe',
}

enum Intval {
  NoShows = 'No Shows',
  The30Minutes = '+30 Minutes',
  Times = 'Times',
}

enum Status {
  Archive = 'archive',
  Live = 'live',
  Preview = 'preview',
}

enum Type {
  Attraction = 'attraction',
  Eventlocation = 'eventlocation',
  Gastronomy = 'gastronomy',
  Hotel = 'hotel',
  Park = 'park',
  Service = 'service',
  Shopping = 'shopping',
  Showlocation = 'showlocation',
  Sight = 'sight',
  Wellness = 'wellness',
}

interface FetchPOI {
  language: string;
  pois: POI[];
}

export { ConnectionOptions, POI, FetchPOI };
