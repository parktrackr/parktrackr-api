import Park from '../park';
import dotenv from 'dotenv';
import { Fetch, Item } from './hellendoorn.types';
import { ProcessedPOI } from '../park.types';
import { Translation } from '../../../mongoose/model';
dotenv.config();

let data: null | Fetch = null;

if (process.env.DATA_HELLENDOORN && process.env.ENVIRONMENT !== 'GITLAB') {
  data = require('../../../data/hellendoorn.json');
} else {
  data = require('../../../mocks/json/hellendoorn/poi.json');
}

/**
 * Class representing the Avonturenpark Hellendoorn park.
 * @extends Park
 */
class Hellendoorn extends Park {
  name: string;
  timezone: string;
  latitude: number;
  longitude: number;
  mainLanguage: string;
  languageOptions: string[];

  /**
   * Create the Avonturenpark Hellendoorn park.
   */
  constructor() {
    super();

    this.name = 'Avonturenpark Hellendoorn';
    this.timezone = 'Europe/Amsterdam';

    this.latitude = 52.38968491328484;
    this.longitude = 6.436094234881132;

    this.mainLanguage = 'nl-NL';
    this.languageOptions = ['en-GB', 'de-DE', 'nl-NL'];
  }

  /**
   * Fetch all POIS
   *
   * @return {typeof data} All Hellendoorn POIS
   */
  private fetchPOIS(): Item[] {
    if (!data) {
      throw new Error();
    }
    return data.Item;
  }

  /**
   * Filter POIS
   * @param {FetchPOI[]} arrays - The arrays with data
   *
   * @returns {FetchPOI[]} The filtered FetchPOIS
   */
  private filterPOIS(array: Item[]): Item[] {
    return array.filter((poi: Item) => poi.Category === 1121 || 1120 || 1113);
  }

  /**
   * Process POIS
   * @param {FetchPOI[]} arrays - The arrays with data
   *
   * @returns {ProcessedPOI[]} The processed POIs
   */
  private processPOIS(arrays: Item[]): ProcessedPOI[] {
    return arrays.map(
      (poi: Item): ProcessedPOI => {
        const { _id, Name, Location } = poi;
        const originalName = Name['nl-NL'];
        const [latitude, longitude] = Location.split(',');
        if (!originalName) {
          throw new Error();
        }
        const translationsObject = this.processTranslations(poi);

        return {
          id: `${_id}`,
          originalName,
          slug: this.sluggify(originalName),
          parkId: this.sluggify(this.name),
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          ...(translationsObject ? { translations: translationsObject } : {}),
        };
      },
    );
  }

  /**
   * Process Translation
   * @param {POI} poi - The poi
   *
   * @returns {Translation} The processed POI Translation
   */
  private processTranslations = (poi: Item): Translation[] => {
    return this.languageOptions.map((lang: string) => {
      const { Name } = poi;
      const name = Name[lang];
      const [language] = lang.split('-');
      if (!name) {
        throw new Error();
      }
      return {
        language,
        name,
      };
    });
  };
  /**
   * Load Attraction POIS
   *
   * @returns {Promise<ProcessedPOI[]>} Attraction POIS
   */
  async loadAttractionPOIS(): Promise<ProcessedPOI[]> {
    const data = await this.fetchPOIS();
    const filtered = this.filterPOIS(data);
    const processed = this.processPOIS(filtered);
    return processed;
  }
}

export default Hellendoorn;
