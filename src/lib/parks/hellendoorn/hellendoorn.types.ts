interface Fetch {
  Resort: Resort[];
  Item: Item[];
  Entrance: Entrance[];
  QueueLine: QueueLine[];
  InfoGroup: InfoGroup[];
  InfoItem: InfoItem[];
  Image: Image[];
  Feature: Feature[];
  Category: Category[];
  Classification: [];
  Offer: [];
  FeedbackReason: [];
  FoodCollection: FoodCollection[];
  FoodGroup: FoodGroup[];
  FoodItem: FoodItem[];
}

interface Category {
  _id: number;
  Name: Name;
  Icon: string;
  Parent: number | null;
}

interface Name {
  [key: string]: string | null;
}

interface Entrance {
  _id: number;
  Location: string;
  Item: number;
}

interface Feature {
  _id: number;
  Title: Name | string;
  Summary: Name | string;
  Image: string;
  URL: Name | string;
}

interface FoodCollection {
  _id: number;
  Name: Name;
  FoodPOI: number;
}

interface FoodGroup {
  _id: number;
  Name: Name;
  FoodCollection: number;
}

interface FoodItem {
  _id: number;
  SKU: string;
  Name: Name | string;
  Summary: Name | null;
  Price: string;
  IsVisible: boolean;
  SuggestDrink?: boolean;
  SuggestSide?: boolean;
  SuggestSauce?: boolean;
  FreeDrink?: boolean;
  FoodGroups: string[];
  Upsell?: number;
  UpsellType?: string;
  SideOption?: boolean;
  MealPrice?: string;
  SuggestTopping?: boolean;
  DrinkOption?: boolean;
  CrossSellMode?: string;
  Tag?: Tag;
  SauceOption?: boolean;
  ToppingOption?: boolean;
}

enum Tag {
  Burrito = 'burrito',
  HotChocolate = 'hot-chocolate',
  Nachos = 'nachos',
}

interface Image {
  _id: number;
  Item: number;
  Image: string;
}

interface InfoGroup {
  _id: number;
  Name: Name;
}

interface InfoItem {
  _id: number;
  Group: number;
  Name: Name;
  URL?: Name;
  Content?: string;
  Path?: Name;
}

interface Item {
  _id: number;
  Name: Name;
  Summary: Name | null;
  Keywords: null;
  DefaultImage: number | null;
  Location: string;
  Featured: boolean;
  WayfindingEnabled: boolean;
  VisibleOnMap: boolean;
  Category: number;
  Parent: null;
  entityIds: [];
  Classifications: [];
  MinimumHeightRequirement?: number | null;
  MinimumUnaccompaniedHeightRequirement?: number | null;
  MaximumHeightRequirement?: number | null;
  MinimumAgeRequirement?: null;
  MinimumUnaccompaniedAgeRequirement?: null;
  MaximumAgeRequirement?: null;
  RestrictionSummary?: Name;
  ShowTimes?: string;
}

interface QueueLine {
  _id: number;
  Item: number;
  Type: string;
}

interface Resort {
  _id: number;
  Name: Name;
  FeedbackURL: null;
  TripAdvisorURL: null;
  TicketsURL: null;
  PrivacyPolicyURL: Name;
  OptInMessage: Name;
  Region: string;
  TimeZone: string;
  Country: string;
  DirectionsLocation: string;
  DefaultMapFocalRegion: string;
}

export { Fetch, Item };
