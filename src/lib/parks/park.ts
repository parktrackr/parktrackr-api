import Entity from './entity';

// TYPES
import { CustomError } from './park.types';
abstract class Park extends Entity {
  constructor(options = {}) {
    super(options);
  }

  /**
   * Is the parameter present
   * @param {string | number | undefined | null} param - language selector
   *
   * @returns {void}
   */
  isParameterPresent = (param: string | number | undefined | null): boolean => {
    if (param === undefined || param === null) {
      return false;
    } else {
      return true;
    }
  };

  /**
   * Is the language available
   * @param {string} lang - language selector
   *
   * @returns {void}
   */
  isLanguageAvailable = (lang: string): boolean => {
    return this.languageOptions.some((l: string) => l === lang);
  };

  /**
   * Throws error with code
   * @param {string} message - Information about the error
   * @param {string} code - The error code
   *
   * @returns {void}
   */
  throwCustomError = (message: string, code: string): void => {
    const error = new Error(message) as CustomError;
    error.code = code;
    throw error;
  };

  /**
   * Throws error if an environmental variable is not present
   * @param {(undefined | null | string)[])} array - Array with environmental variables
   *
   * @returns {void}
   */
  validateEnvironmentalVariables = (array: (undefined | null | string)[]): void => {
    array.forEach((env: undefined | null | string) => {
      if (env === undefined || env == null) {
        this.throwCustomError(
          `Environmental variable missing: ${Object.keys({ env })[0]}, in class: ${this.constructor.name}.`,
          'ENVVARIABLEMISSING',
        );
      }
    });
  };

  /**
   * Sluggifies a string.
   * @param {string} string - String that needs to be sluggified
   *
   * @returns {string} Sluggified string
   */
  sluggify = (string: string): string => {
    return string
      .toLowerCase()
      .replace(/ /g, '-')
      .replace('ö', 'o')
      .replace('ä', 'a')
      .replace('ß', 'ss')
      .replace('„', '')
      .replace('“', '');
  };
}

export default Park;
