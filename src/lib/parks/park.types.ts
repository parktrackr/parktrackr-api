type ProcessedPOI = {
  id: string;
  originalName: string;
  slug: string;
  parkId: string;
  latitude: number;
  longitude: number;
};

type Translation = {
  language: string;
  name: string;
};

interface CustomError extends Error {
  code?: string;
}

export { ProcessedPOI, Translation, CustomError };
