import fetch from 'node-fetch';
import Park from '../park';
import dotenv from 'dotenv';
dotenv.config();

// TYPES
import { ConnectionOptions, POI } from './phantasialand.types';
import { ProcessedPOI } from '../park.types';
import { Translation } from '../../../mongoose/model';

/**
 * Class representing the Phantasialand park.
 * @extends Park
 */
class Phantasialand extends Park {
  name: string;
  timezone: string;
  latitude: number;
  longitude: number;
  mainLanguage: string;
  languageOptions: string[];
  /**
   * Create the Phantasialand park.
   * @param {ConnectionOptions} options
   */
  constructor(options: ConnectionOptions) {
    super(options);

    this.name = 'Phantasialand';
    this.timezone = 'Europe/Berlin';

    // Latitude and longitude positioned at the entrance of the park
    this.latitude = 50.79910004787788;
    this.longitude = 6.879318864300787;

    // language options
    this.mainLanguage = 'de';
    this.languageOptions = ['en', 'fr', 'de', 'nl'];

    const envVariables = [this.config.apiURL, this.config.waitTimesURL, this.config.accesToken, this.config.loc];

    this.validateEnvironmentalVariables(envVariables);
  }

  /**
   * Fetch all POIS
   *
   * @return {Promise<POI[]>} All POIS
   */
  async fetchPOIS(): Promise<POI[]> {
    return fetch(this.config.apiURL + this.config.accesToken, {
      method: 'GET',
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        this.throwCustomError(`Fetch Error: ${err.message}`, 'FETCHABORTED');
      });
  }

  /**
   * Filter POIS
   * @param {FetchPOI[]} arrays - The arrays with data
   * @param {string} filter - the filter string

   *
   * @returns {FetchPOI[]} The filtered FetchPOIS
   */
  private filterPOIS(array: POI[], filter: string): POI[] {
    return array.filter((poi: POI) => poi.category === filter);
  }

  /**
   * Process POIS
   * @param {FetchPOI} array - The array with data
   *
   * @returns {ProcessedPOI[]} The processed POIs
   */
  private processPOIS(array: POI[]): ProcessedPOI[] {
    return array.map(
      (poi: POI): ProcessedPOI => {
        const { poiNumber, title, entrance } = poi;
        const { world } = entrance;
        const { lat, lng } = world;
        if (!title.de) {
          throw new Error('');
        }

        let id = null;

        if (poiNumber === 600 && title.de === 'Winja‘s Force') {
          id = '600-2';
        }

        const translationsObject = this.processTranslations(poi);

        return {
          id: id ? id : `${poiNumber}`,
          originalName: title.de,
          slug: this.sluggify(title.de),
          parkId: this.sluggify(this.name),
          latitude: lat,
          longitude: lng,
          ...(translationsObject ? { translations: translationsObject } : {}),
        };
      },
    );
  }

  /**
   * Process Translation
   * @param {POI} poi - The poi
   *
   * @returns {Translation} The processed POI Translation
   */
  private processTranslations = (poi: POI): Translation[] => {
    return this.languageOptions.map((lang: string) => {
      const { title } = poi;
      const name = title[lang];

      if (!name) {
        throw new Error('');
      }

      return {
        language: lang,
        name,
      };
    });
  };

  /**
   * Load Attraction POIS
   *
   * @returns {Promise<ProcessedPOI[]>} Attraction POIS
   */
  async loadAttractionPOIS(): Promise<ProcessedPOI[]> {
    const data = await this.fetchPOIS();
    const filtered = this.filterPOIS(data, 'ATTRACTIONS');
    const processed = this.processPOIS(filtered);
    return processed;
  }
}

export default Phantasialand;
