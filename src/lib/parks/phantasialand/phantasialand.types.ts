interface ConnectionOptions {
  apiURL: string;
  waitTimesURL: string;
  accesToken: string;
  loc: string;
}

interface POI {
  poiNumber: number | null;
  minAge: number | null;
  maxAge: number | null;
  minSize: number | null;
  maxSize: number | null;
  minSizeEscort: number | null;
  tags: string[];
  category: Category;
  slug: null | string;
  area: Area;
  parkMonitorReferenceName: null | string;
  seasons: Season[];
  navigationEnabled: boolean;
  id: number;
  createdAt: Date;
  updatedAt: Date;
  titleImage: TitleImage;
  title: Translatable;
  tagline: Translatable;
  description: Translatable;
  entrance: Entrance;
  exit: Entrance | null;
  preferredDestinations: Entrance[];
  weblink: Translatable | null;
}

enum Area {
  Berlin = 'BERLIN',
  ChinaTown = 'CHINA_TOWN',
  DeepInAfrica = 'DEEP_IN_AFRICA',
  Fantasy = 'FANTASY',
  Mexico = 'MEXICO',
  Mystery = 'MYSTERY',
}

enum Category {
  Attractions = 'ATTRACTIONS',
  EventLocations = 'EVENT_LOCATIONS',
  PhantasialandHotels = 'PHANTASIALAND_HOTELS',
  PhantasialandHotelsBars = 'PHANTASIALAND_HOTELS_BARS',
  PhantasialandHotelsRestaurants = 'PHANTASIALAND_HOTELS_RESTAURANTS',
  RestaurantsAndSnacks = 'RESTAURANTS_AND_SNACKS',
  Service = 'SERVICE',
  Shops = 'SHOPS',
  Shows = 'SHOWS',
  TheSixDragons = 'THE_SIX_DRAGONS',
}

interface Translatable {
  [key: string]: null | string;
}

interface Entrance {
  id: null | string;
  world: Map;
  map: Map;
}

interface Map {
  lat: number;
  lng: number;
}

enum Season {
  Summer = 'SUMMER',
  Winter = 'WINTER',
}

interface TitleImage {
  id: number;
  url: string;
}

export { ConnectionOptions, POI };
