import fetch from 'node-fetch';
import Park from '../park';
import dotenv from 'dotenv';
dotenv.config();

// TYPES
import { ConnectionOptions, POI } from './toverland.types';
import { ProcessedPOI } from '../park.types';
import { Translation } from '../../../mongoose/model';

/**
 * Class representing the Toverland park.
 * @extends Park
 */
class Toverland extends Park {
  name: string;
  timezone: string;
  latitude: number;
  longitude: number;
  mainLanguage: string;
  languageOptions: string[];
  /**
   * Create the Toverland park.
   * @param {ConnectionOptions} options - The variables used for the API
   */
  constructor(options: ConnectionOptions) {
    super(options);

    this.name = 'Toverland';
    this.timezone = 'Europe/Amsterdam';

    // Latitude and longitude positioned at the entrance of the park
    this.latitude = 51.397673285726114;
    this.longitude = 5.981651557822892;

    // language options
    this.mainLanguage = 'nl';
    this.languageOptions = ['en', 'fr', 'de', 'nl'];

    const envVariables = [this.config.apiAuthKey, this.config.apiHostURL, this.config.openingHoursURL];

    this.validateEnvironmentalVariables(envVariables);
  }

  /**
   * Fetch all POIS
   *
   * @return {Promise<POI[]>} All POIS
   */
  async fetchAttractionPOIS(): Promise<POI[]> {
    return fetch(this.config.apiHostURL + '/park/ride/list', {
      method: 'GET',
      headers: {
        Authorization: this.config.apiAuthKey,
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        this.throwCustomError(`Fetch Error: ${err.message}`, 'FETCHABORTED');
      });
  }

  /**
   * Process POIS
   * @param {FetchPOI} array - The array with data
   *
   * @returns {ProcessedPOI[]} The processed POIs
   */
  private processPOIS(array: POI[]): ProcessedPOI[] {
    return array.map(
      (poi: POI): ProcessedPOI => {
        const { id, name, latitude, longitude } = poi;

        const translationsObject = this.processTranslations(poi);

        return {
          id: `${id}`,
          originalName: name,
          slug: this.sluggify(name),
          parkId: this.sluggify(this.name),
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          ...(translationsObject ? { translations: translationsObject } : {}),
        };
      },
    );
  }

  /**
   * Process Translation
   * @param {POI} poi - The poi
   *
   * @returns {Translation} The processed POI Translation
   */
  private processTranslations = (poi: POI): Translation[] => {
    return this.languageOptions.map((lang: string) => {
      const { name } = poi;
      return {
        language: lang,
        name,
      };
    });
  };

  /**
   * Load Attraction POIS
   *
   * @returns {Promise<ProcessedPOI[]>} Attraction POIS
   */
  async loadAttractionPOIS(): Promise<ProcessedPOI[]> {
    const data = await this.fetchAttractionPOIS();
    const processed = this.processPOIS(data);
    return processed;
  }
}

export default Toverland;
