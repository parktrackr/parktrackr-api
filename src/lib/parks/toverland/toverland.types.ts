interface ConnectionOptions {
  apiAuthKey: string;
  apiHostURL: string;
  openingHoursURL: string;
}

interface POI {
  id: number;
  name: string;
  area_id: string;
  latitude: string;
  longitude: string;
  short_description: Description;
  description: Description;
  thumbnail: string;
  minLength: string;
  supervision: string | null;
  header_description: Description;
}

interface Description {
  nl: string;
  de: string;
  en: string;
}

export { ConnectionOptions, POI };
