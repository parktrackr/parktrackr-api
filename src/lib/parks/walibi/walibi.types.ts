interface ConnectionOptions {
  entertainmentURL: string;
}

interface POIData {
  wc: Service[];
  entertainment: Entertainment[];
  restaurant: Restaurant[];
  shop: Restaurant[];
  show: [];
  service: Service[];
  halloween?: Halloween[];
}

interface Entertainment {
  uuid: string;
  title: string;
  image: Image;
  location: Location;
  status: Status;
  waitingTime: number;
  waitingTimeDescription?: string;
  parameters: Parameter[];
  category: EntertainmentCategory;
  additionalContent: EntertainmentAdditionalContent[];
  video?: string;
  fastPass?: boolean;
  photoService?: boolean;
  isNew?: boolean;
  reopeningDate?: Date;
  babySwitch?: boolean;
}

interface EntertainmentAdditionalContent {
  title?: string;
  text?: string;
  image: Image;
}

interface Image {
  url: string;
  thumbnailUrl: string;
}

interface EntertainmentCategory {
  id: string;
  name: string;
  weight: number;
  icon: PurpleIcon;
}

interface PurpleIcon {
  iconType: IconType;
  imageUrl: string;
}

enum IconType {
  SVG = 'svg',
}

interface Location {
  lat: string;
  lon: string;
}

interface Parameter {
  title: string;
  value: string;
}

enum Status {
  Closed = 'closed',
  Open = 'open',
  OutOfOrder = 'outOfOrder',
}

interface Halloween {
  uuid: string;
  title: string;
  image: Image;
  location: Location;
  status: Status;
  waitingTime: number;
  parameters: Parameter[];
  category: HalloweenCategory;
  additionalContent: HalloweenAdditionalContent[];
  halloweenParameters: HalloweenParameters;
  video?: string;
  waitingTimeDescription?: string;
}

interface HalloweenAdditionalContent {
  text: string;
  image: Image;
}

interface HalloweenCategory {
  id: string;
  name: string;
  weight: number;
  icon: FluffyIcon;
}

interface FluffyIcon {
  iconType: string;
  imageUrl: string;
  smallThumbnail?: string;
  largeThumbnail?: string;
}

interface HalloweenParameters {
  fear: number;
  priceInfoTitle?: string;
  priceInfoDescription?: string;
  scheduleInfoTitle?: string;
  scheduleInfoDescription?: string;
}

interface Restaurant {
  uuid: string;
  title: string;
  image: Image;
  location?: Location;
  status: Status;
  waitingTime: number;
  parameters: Parameter[];
  category: EntertainmentCategory;
  menu?: Menu[];
  additionalContent: EntertainmentAdditionalContent[];
  isNew?: boolean;
  video?: string;
  schedule?: string;
}

interface Menu {
  title: string;
  items: Item[];
}

interface Item {
  prefix: null | string;
  label: string;
  price: string;
}

interface Service {
  uuid: string;
  code: string;
  title: string;
  description?: string;
  image: Image;
  category: EntertainmentCategory;
  location: Location;
  additionalContent: EntertainmentAdditionalContent[];
  status: Status;
}

interface FetchPOI {
  language: string;
  data: Entertainment[];
}

export { ConnectionOptions, POIData, FetchPOI, Entertainment };
