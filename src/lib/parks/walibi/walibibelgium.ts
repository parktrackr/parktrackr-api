import fetch from 'node-fetch';
import Park from '../park';
import dotenv from 'dotenv';
dotenv.config();

// TYPES
import { ConnectionOptions, Entertainment, FetchPOI, POIData } from './walibi.types';
import { ProcessedPOI } from '../park.types';
import { Translation } from '../../../mongoose/model';

/**
 * Class representing the Walibi Belgium park.
 * @extends Park
 */
class WalibiBelgium extends Park {
  name: string;
  timezone: string;
  latitude: number;
  longitude: number;
  mainLanguage: string;
  languageOptions: string[];

  /**
   * Create the Walibi Belgium park.
   * @param {ConnectionOptions} options
   */
  constructor(options: ConnectionOptions) {
    super(options);

    this.name = 'Walibi Belgium';
    this.timezone = 'Europe/Amsterdam';

    // Latitude and longitude positioned at the entrance of the park
    this.latitude = 50.70193444815738;
    this.longitude = 4.594061025043646;

    // language options
    this.mainLanguage = 'nl';
    this.languageOptions = ['en', 'nl', 'fr'];

    const envVariables = [this.config.entertainmentURL];

    this.validateEnvironmentalVariables(envVariables);
  }

  /**
   * Fetch POI
   * @param {string} lang -  language selector
   *
   * @return {Promise<FetchPOI | null>} -  Walibi Belgium POI
   */
  private async fetchPOI(lang: string): Promise<FetchPOI> {
    return fetch(this.config.entertainmentURL.replace('LANG', lang), {
      method: 'GET',
    })
      .then((response) => {
        return response.json();
      })
      .then((data: POIData) => {
        return {
          language: lang,
          data: data.entertainment,
        };
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  }

  /**
   * Fetch POIS
   *
   * @returns {Promise<FetchPOI[]>} The fetched data
   */
  private async fetchPOIS(): Promise<FetchPOI[]> {
    const promises: Promise<FetchPOI>[] = [];

    this.languageOptions.forEach((lang: string) => {
      promises.push(this.fetchPOI(lang));
    });

    return Promise.all(promises);
  }

  /**
   * Process POIS
   * @param {FetchPOI[]} arrays - The arrays with data
   *
   * @returns {ProcessedPOI[]} The processed POIs
   */
  private processPOIS(arrays: FetchPOI[]): ProcessedPOI[] {
    const mainArray = arrays.find((array: FetchPOI) => array.language === this.mainLanguage);

    if (!mainArray) {
      throw new Error();
    }

    return mainArray.data.map(
      (poi: Entertainment): ProcessedPOI => {
        const { uuid, title, location } = poi;
        const { lat, lon } = location;

        const translationsObject = this.processTranslations(uuid, arrays);

        return {
          id: uuid,
          originalName: title,
          slug: this.sluggify(title),
          parkId: this.sluggify(this.name),
          latitude: parseFloat(lat),
          longitude: parseFloat(lon),
          ...(translationsObject ? { translations: translationsObject } : {}),
        };
      },
    );
  }
  /**
   * Process Translations
   * @param {string} id - identifier of the item
   * @param {FetchPOI[]} arrays - The translation arrays
   *
   * @returns {(Translation | undefined)[]} The processed POI Translations
   */
  private processTranslations(id: string, arrays: FetchPOI[]): (Translation | undefined)[] {
    return arrays.map((transArray: FetchPOI) => {
      const item = transArray.data.find((tr: Entertainment) => tr.uuid === id);
      if (!item) {
        return;
      }
      const { title } = item;
      return {
        language: transArray.language,
        name: title,
      };
    });
  }

  /**
   * Load Attraction POIS
   *
   * @returns {Promise<ProcessedPOI[]>} Attraction POIS
   */
  async loadAttractionPOIS(): Promise<ProcessedPOI[]> {
    const data = await this.fetchPOIS();
    const processed = this.processPOIS(data);
    return processed;
  }
}

export default WalibiBelgium;
