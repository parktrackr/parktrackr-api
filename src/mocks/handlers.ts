import { rest } from 'msw';
import dotenv from 'dotenv';
dotenv.config();

// Efteling
import eftelingPOINLJSON from './json/efteling/poi-nl.json';
import eftelingPOIDEJSON from './json/efteling/poi-de.json';
import eftelingPOIENJSON from './json/efteling/poi-en.json';
import eftelingPOIFRJSON from './json/efteling/poi-fr.json';

// Europa-Park
import europaParkLoginJSON from './json/europapark/login.json';
import europaParkRefreshJSON from './json/europapark/refresh.json';
import europaParkPOIDEJSON from './json/europapark/poi-de.json';

// Phantasialand
import phantasialandPOIJSON from './json/phantasialand/poi.json';

// Toverland
import toverlandPOIJSON from './json/toverland/poi.json';

// Walibi
import walibiPOINLJSON from './json/walibi/poi-nl.json';

export const handlers = [
  rest.get(
    process.env.EFTELING_SEARCH_URL
      ? process.env.EFTELING_SEARCH_URL + 'search?'
      : 'http://parktrackr.nl/mock/efteling/poi/search?',
    (req, res, ctx) => {
      if (req.url.search.includes('language%20%27$nl')) {
        return res(ctx.status(200), ctx.json(eftelingPOINLJSON));
      } else if (req.url.search.includes('language%20%27$de')) {
        return res(ctx.status(200), ctx.json(eftelingPOIDEJSON));
      } else if (req.url.search.includes('language%20%27$en')) {
        return res(ctx.status(200), ctx.json(eftelingPOIENJSON));
      } else if (req.url.search.includes('language%20%27$fr')) {
        return res(ctx.status(200), ctx.json(eftelingPOIFRJSON));
      }
      return res(ctx.status(404));
    },
  ),

  rest.post(
    process.env.EUROPAPARK_LOGIN_URL ? process.env.EUROPAPARK_LOGIN_URL : 'http://parktrackr.nl/mock/europa-park/login',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(europaParkLoginJSON));
    },
  ),

  rest.post(
    process.env.EUROPAPARK_REFRESH_URL
      ? process.env.EUROPAPARK_REFRESH_URL
      : 'http://parktrackr.nl/mock/europa-park/refresh',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(europaParkRefreshJSON));
    },
  ),

  rest.get(
    process.env.EUROPAPARK_API_URL
      ? process.env.EUROPAPARK_API_URL + 'de'
      : 'http://parktrackr.nl/mock/europa-park/poi/de',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(europaParkPOIDEJSON));
    },
  ),

  rest.get(
    process.env.PHANTASIALAND_API_URL && process.env.PHANTASIALAND_ACCES_TOKEN
      ? process.env.PHANTASIALAND_API_URL + process.env.PHANTASIALAND_ACCES_TOKEN
      : 'http://parktrackr.nl/mock/phantasialand/poi/' + 'acces-token',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(phantasialandPOIJSON));
    },
  ),

  rest.get(
    process.env.TOVERLAND_API_URL
      ? process.env.TOVERLAND_API_URL + '/park/ride/list'
      : 'http://parktrackr.nl/mock/toverland/poi/park/ride/list',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(toverlandPOIJSON));
    },
  ),

  rest.get(
    process.env.WALIBIHOLLAND_ENTERTAINMENTS_URL
      ? process.env.WALIBIHOLLAND_ENTERTAINMENTS_URL.replace('LANG', 'nl')
      : 'http://parktrackr.nl/mock/walibi/poi',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(walibiPOINLJSON));
    },
  ),
];
