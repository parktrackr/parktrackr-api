import { getModelForClass, index, prop } from '@typegoose/typegoose';

/**
 * Class representing the Translation Model
 */

class Translation {
  /**
   * prop representing the language of the Translation
   */
  @prop({
    required: [true, 'language is required.'],
    enum: ['en', 'nl', 'de', 'fr'],
  })
  public language!: string;

  /**
   * prop representing the name of the Attraction in the Translation
   */
  @prop({
    required: [true, 'name is required.'],
    validate: {
      validator(v: string) {
        if (!/^[a-zA-Z0-9-_&ñ\'øå:\"!'ü+\’Öíé–\‘\.ößä„“â«»èàüÀ-ž\, ]+$/.test(v)) {
          console.log(v);
        }
        return /^[a-zA-Z0-9-_&ñ\'øå:\"!'ü+\’Öíé–\‘\.ößä„“â«»èàüÀ-ž\, ]+$/.test(v);
      },
      message: `translatable name can only have letters, numbers, -, _ and spaces.`,
    },
  })
  public name!: string;
}

/**
 * Class representing the Attraction Model
 */
@index({ id: 1, parkId: 1 }, { unique: true })
class Attraction {
  /**
   * prop representing the id of the Attraction
   */
  @prop({
    required: [true, 'id is required.'],
    lowercase: true,
    trim: true,
    validate: {
      validator(v: string) {
        return /^[a-z0-9-_&ñ\'øå:\"!'ü+\’íé–\‘\. ]+$/.test(v);
      },
      message: 'id can only have letters, numbers, - and _., and should be AttractionID.',
    },
  })
  public id!: string;

  /**
   * prop representing the name of the Attraction in the mainLanguage
   */
  @prop({
    required: [true, 'originalName is required.'],
    trim: true,
    validate: {
      validator(v: string) {
        if (!/^[a-zA-Z0-9-_&ñ\'øå:\"!'ü+\’Öíé–\‘\.ößä„“ ]+$/.test(v)) {
          console.log(v);
        }
        return /^[a-zA-Z0-9-_&ñ\'øå:\"!'ü+\’Öíé–\‘\.ößä„“ ]+$/.test(v);
      },
      message: 'originalName can only have letters, numbers, -, _ and spaces.',
    },
  })
  public originalName!: string;

  /**
   * prop representing the id of the Attraction
   */
  @prop({
    required: [true, 'slug is required.'],
    lowercase: true,
    trim: true,
    validate: {
      validator(v: string) {
        if (!/^[a-z0-9-_&ñ\'øå:\"!'ü+\’íé–\‘\. ]+$/.test(v)) {
          console.log(v);
        }
        return /^[a-z0-9-_&ñ\'øå:\"!'ü+\’íé–\‘\. ]+$/.test(v);
      },
      message: 'id can only have letters, numbers, - and _., and should be AttractionID.',
    },
  })
  public slug!: string;

  /**
   * prop representing the id of the park of the Attraction
   */
  @prop({
    required: [true, 'parkId is required.'],
    lowercase: true,
    trim: true,
    validate: {
      validator(v: string) {
        return /^[a-z0-9-_&ñ\'øå:\"!'ü+\’íé–\‘\.ô ]+$/.test(v);
      },
      message: 'id can only have letters, numbers, - and _., and should be ParkID.',
    },
  })
  public parkId!: string;

  /**
   * prop representing the latitude of the Attraction
   */
  @prop({
    required: [true, 'latitude is required.'],
    validate: {
      validator(v: number) {
        if (v > 90 || v < -90) {
          return false;
        }
        return true;
      },
      message: 'latitude should exist of (XX)X.XXXXX.',
    },
  })
  public latitude!: number;

  /**
   * prop representing the longitude of the Attraction
   */
  @prop({
    required: [true, 'longitude is required.'],
    validate: {
      validator(v: number) {
        if (v > 180 || v < -180) {
          return false;
        }
        return true;
      },
      message: 'longitude should exist of (XX)X.XXXXX.',
    },
  })
  public longitude!: number;

  /**
   * prop array of Translation objects, representing translatable data
   */
  @prop({
    type: [Translation],
  })
  public translations!: Translation[];
}

/**
 * Creating model Attraction, based on Attraction class.
 */
const AttractionModel = getModelForClass(Attraction, {
  schemaOptions: { collection: 'attractions', timestamps: true },
});

export { Attraction, AttractionModel, Translation };
