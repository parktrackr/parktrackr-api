import { getModelForClass, prop } from '@typegoose/typegoose';

/**
 * Class representing the Temp Model
 */
class Temp {
  /**
   * prop representing the id of the temporary doc
   */
  @prop({
    required: [true, 'id is required.'],
    unique: true,
  })
  public id!: string;

  /**
   * prop representing the value of a temporary doc
   */
  @prop({
    required: [true, 'value is required.'],
  })
  public value!: string;
}

/**
 * Creating model Temp, based on Temp class.
 */
const TempModel = getModelForClass(Temp, {
  schemaOptions: { collection: 'temps', timestamps: true },
});

export { Temp, TempModel };
