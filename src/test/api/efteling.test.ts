// import { assert, expect } from 'chai';
// import api from '../../lib';
// import * as connections from '../../lib/parks/connections';
// import { POI } from '../../lib/parks/efteling/efteling.types';

// const park = new api.parks.Efteling(connections.Efteling());

// const assertPOI = (poi: POI) => {
//   const { id, fields } = poi;
//   const { name, language, latlon } = fields;

//   assert(typeof id === 'string', 'id is not a String.');
//   assert(id.includes(`-${language}`), 'id does not specify language.');

//   assert(typeof fields.id === 'string', 'field id is not a String.');
//   assert(id === `${fields.id}-${language}`, 'field id does not match id.');

//   assert(typeof name === 'string', 'name is not a String.');

//   assert(typeof latlon === 'string', 'latlon is not a String.');
// };

// describe('Efteling', () => {
//   context('Fetch', () => {
//     it('POIs Dutch (nl)', async () => {
//       const data = await park.fetchPOI('nl');
//       assert(data.language === 'nl', 'POI Language does not match provided language.');
//       data.pois.every((poi: POI) => assertPOI(poi));
//     });

//     it('POIs English (en)', async () => {
//       const data = await park.fetchPOI('en');
//       assert(data.language === 'en', 'POI Language does not match provided language.');
//       data.pois.every((poi: POI) => assertPOI(poi));
//     });

//     it('POIs German (de)', async () => {
//       const data = await park.fetchPOI('de');
//       assert(data.language === 'de', 'POI Language does not match provided language.');
//       data.pois.every((poi: POI) => assertPOI(poi));
//     });

//     it('POIs French (fr)', async () => {
//       const data = await park.fetchPOI('fr');
//       assert(data.language === 'fr', 'POI Language does not match provided language.');
//       data.pois.every((poi: POI) => assertPOI(poi));
//     });

//     it('POIs Error Trapping (empty)', async () => {
//       try {
//         await park.fetchPOI('');
//       } catch (error) {
//         assert(
//           error.message === 'Missing language in langoptions. en,fr,de,nl ',
//           `Error Message does not match assertion. : ${error.message}`,
//         );
//         return;
//       }
//       expect.fail('Should have thrown.');
//     });

//     it('POIs Error Trapping (ru)', async () => {
//       try {
//         await park.fetchPOI('ru');
//       } catch (error) {
//         assert(
//           error.message === 'Missing language in langoptions. en,fr,de,nl ru',
//           `Error Message does not match assertion. : ${error.message}`,
//         );
//         return;
//       }
//       expect.fail('Should have thrown.');
//     });

//     it('POIs Multiple (en, de, nl)', async () => {
//       const multiplePois = await park.fetchPOIs(['en', 'de', 'nl']);
//       assert(multiplePois[0].language === 'en', 'POI Language does not match provided language.');
//       assert(multiplePois[1].language === 'de', 'POI Language does not match provided language.');
//       assert(multiplePois[2].language === 'nl', 'POI Language does not match provided language.');

//       multiplePois.every((data) => {
//         return data.pois.every((poi: POI) => assertPOI(poi));
//       });
//     });

//     it('POIs Multiple Error Trapping (empty)', async () => {
//       try {
//         await park.fetchPOIs([]);
//       } catch (error) {
//         assert(error.message === 'No languages provided.', 'Error Message does not match assertion.');
//         return;
//       }
//       expect.fail('Should have thrown.');
//     });

//     it('POIs Multiple Error Trapping (nl, ru, de)', async () => {
//       try {
//         await park.fetchPOIs(['nl', 'ru', 'de']);
//       } catch (error) {
//         assert(
//           error.message === 'Missing language in langoptions. en,fr,de,nl ru',
//           `Error Message does not match assertion. : ${error.message}`,
//         );
//         return;
//       }
//       expect.fail('Should have thrown.');
//     });
//   });
// });
