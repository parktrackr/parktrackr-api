// import { assert } from 'chai';
// import api from '../../lib';
// import * as connections from '../../lib/parks/connections';
// import { POI } from '../../lib/parks/europapark/europapark.types';

// const park = new api.parks.EuropaPark(connections.EuropaPark());

// const assertPOI = (poi: POI) => {
//   const { id } = poi;

//   assert(typeof id === 'number', 'id is not a Number.');
// };

// describe('Europa-Park', () => {
//   context('Fetch', () => {
//     it('POIs German (de)', async () => {
//       const data = await park.fetchPOI('de');
//       assert(data.language === 'de', 'POI Language does not match provided language.');
//       data.pois.every((poi: POI) => assertPOI(poi));
//     });
//   });
// });
