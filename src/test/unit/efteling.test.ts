// import { assert, expect } from 'chai';
// import api from '../../lib';
// import * as connections from '../../lib/parks/connections';
// import { POI } from '../../lib/parks/efteling/efteling.types';

// const park = new api.parks.Efteling(connections.Efteling());

// describe('Efteling', () => {
//   context('Fetching', () => {
//     it('Dutch POIS', async () => {
//       const data = await park.fetchPOI('nl');
//       if (!data) {
//         throw new Error('');
//       }
//       assert(data.hits.hit[0].fields.language === 'nl', 'POI Language does not match provided language.');
//     });
//     it('English POIS', async () => {
//       const data = await park.fetchPOI('en');
//       if (!data) {
//         throw new Error('');
//       }
//       assert(data.hits.hit[0].fields.language === 'en', 'POI Language does not match provided language.');
//     });
//     it('German POIS', async () => {
//       const data = await park.fetchPOI('de');
//       if (!data) {
//         throw new Error('');
//       }
//       assert(data.hits.hit[0].fields.language === 'de', 'POI Language does not match provided language.');
//     });
//     it('French POIS', async () => {
//       const data = await park.fetchPOI('fr');
//       if (!data) {
//         throw new Error('');
//       }
//       assert(data.hits.hit[0].fields.language === 'fr', 'POI Language does not match provided language.');
//     });
//     // it('Multiple POIS', async () => {
//     //   const multiplePois = await park.fetchPOIs(['en', 'de', 'nl']);
//     //   assert(multiplePois[0].language === 'en', 'POI Language does not match provided language.');
//     //   assert(multiplePois[1].language === 'de', 'POI Language does not match provided language.');
//     //   assert(multiplePois[2].language === 'nl', 'POI Language does not match provided language.');
//     // });
//   });
//   context('Processing', () => {
//     context('Attraction POI', () => {
//       it('id', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         const { hits } = data;
//         const { hit: pois } = hits;

//         const attractions = pois.filter((poi: POI) => poi.fields.category === 'attraction');
//         attractions.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.id === poi.fields.id);
//         });
//       });
//       it('originalName', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         const { hits } = data;
//         const { hit: pois } = hits;

//         const attractions = pois.filter((poi: POI) => poi.fields.category === 'attraction');
//         attractions.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.originalName === poi.fields.name);
//         });
//       });
//       it('parkId', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         const { hits } = data;
//         const { hit: pois } = hits;

//         const attractions = pois.filter((poi: POI) => poi.fields.category === 'attraction');
//         attractions.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.parkId === 'efteling');
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         const { hits } = data;
//         const { hit: pois } = hits;
//         const attractions = pois.filter((poi: POI) => poi.fields.category === 'attraction');
//         attractions.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           const latlonSplit = poi.fields.latlon.split(',');
//           assert(
//             processed.latitude === parseFloat(latlonSplit[0]),
//             'Processed latitude does not match expected return.',
//           );
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         const { hits } = data;
//         const { hit: pois } = hits;
//         const attractions = pois.filter((poi: POI) => poi.fields.category === 'attraction');
//         attractions.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           const latlonSplit = poi.fields.latlon.split(',');
//           assert(
//             processed.longitude === parseFloat(latlonSplit[1]),
//             'Processed longitude does not match expected return.',
//           );
//         });
//       });
//     });
//     context('Translation', () => {
//       it('language', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         const { hits } = data;
//         const { hit: pois } = hits;

//         const attractions = pois.filter((poi: POI) => poi.fields.category === 'attraction');
//         attractions.forEach((poi: POI) => {
//           const processed = park.processTranslation(poi);
//           assert(processed.language === 'nl');
//         });
//       });
//       it('name', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         const { hits } = data;
//         const { hit: pois } = hits;

//         const attractions = pois.filter((poi: POI) => poi.fields.category === 'attraction');
//         attractions.forEach((poi: POI) => {
//           const processed = park.processTranslation(poi);
//           assert(processed.name === poi.fields.name);
//         });
//       });
//     });
//   });
//   context('Validating', () => {
//     it('fetchPOI with empty string returns error', async () => {
//       try {
//         await park.fetchPOI('');
//       } catch (error) {
//         assert(error.code === 'LANGUAGEUNAVAILABLE', `Error Code does not match assertion. : ${error.code}`);
//         return;
//       }
//       expect.fail('Should have thrown.');
//     });

//     it('fetchPOI with non existing language returns error', async () => {
//       try {
//         await park.fetchPOI('ru');
//       } catch (error) {
//         assert(error.code === 'LANGUAGEUNAVAILABLE', `Error Code does not match assertion. : ${error.code}`);
//         return;
//       }
//       expect.fail('Should have thrown.');
//     });
//     //   it('Error: No languages provided.', async () => {
//     //     try {
//     //       await park.fetchPOIs([]);
//     //     } catch (error) {
//     //       assert(error.message === 'No languages provided.', 'Error Message does not match assertion.');
//     //       return;
//     //     }
//     //     expect.fail('Should have thrown.');
//     //   });

//     //   it('Error: Language array has language not present in API', async () => {
//     //     try {
//     //       await park.fetchPOIs(['nl', 'ru', 'de']);
//     //     } catch (error) {
//     //       assert(
//     //         error.message === 'Missing language in langoptions. en,fr,de,nl ru',
//     //         `Error Message does not match assertion. : ${error.message}`,
//     //       );
//     //       return;
//     //     }
//     //     expect.fail('Should have thrown.');
//   });
// });
