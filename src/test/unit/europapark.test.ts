// import { assert } from 'chai';
// import api from '../../lib';
// import * as connections from '../../lib/parks/connections';
// import { POI } from '../../lib/parks/europapark/europapark.types';

// const park = new api.parks.EuropaPark(connections.EuropaPark());

// describe('Europa-Park', () => {
//   context('Fetching', () => {
//     it('German POI', async () => {
//       const data = await park.fetchPOI('de');
//       if (!data) {
//         throw new Error('');
//       }
//     });
//   });
//   context('Processing', () => {
//     context('Attraction POI', () => {
//       it('id', async () => {
//         const data = await park.fetchPOI('de');
//         if (data === null) {
//           throw new Error('');
//         }
//         data.pois.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.id === `${poi.id}`);
//         });
//       });
//       it('originalName', async () => {
//         const data = await park.fetchPOI('de');
//         if (data === null) {
//           throw new Error('');
//         }
//         data.pois.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.originalName === poi.name);
//         });
//       });
//       it('parkId', async () => {
//         const data = await park.fetchPOI('de');
//         if (data === null) {
//           throw new Error('');
//         }

//         data.pois.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.parkId === 'europa-park');
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOI('de');
//         if (data === null) {
//           throw new Error('');
//         }

//         data.pois.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.latitude === poi.latitude, 'Processed latitude does not match expected return.');
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOI('de');
//         if (data === null) {
//           throw new Error('');
//         }

//         data.pois.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.longitude === poi.longitude, 'Processed longitude does not match expected return.');
//         });
//       });
//     });
//     context('Translation', () => {
//       it('language', async () => {
//         const data = await park.fetchPOI('de');
//         if (data === null) {
//           throw new Error('');
//         }
//         data.pois.forEach((poi: POI) => {
//           const processed = park.processTranslation(poi, data.language);
//           assert(processed.language === data.language);
//         });
//       });
//       it('name', async () => {
//         const data = await park.fetchPOI('de');
//         if (data === null) {
//           throw new Error('');
//         }
//         data.pois.forEach((poi: POI) => {
//           const processed = park.processTranslation(poi, data.language);
//           assert(processed.name === poi.name);
//         });
//       });
//     });
//   });
// });
