// import { assert } from 'chai';
// import api from '../../lib';
// import { Item } from '../../lib/parks/hellendoorn/hellendoorn.types';

// const park = new api.parks.Hellendoorn();

// describe('Avonturenpark Hellendoorn', () => {
//   context('Fetching', () => {
//     it('POIS', async () => {
//       const data = park.fetchPOIs();
//       if (!data) {
//         throw new Error('');
//       }
//       assert(data.Item.length);
//     });
//   });
//   context('Processing', () => {
//     context('Attraction POI', () => {
//       it('id', async () => {
//         const data = park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.Item.forEach((poi: Item) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.id === `${poi._id}`);
//         });
//       });
//       it('originalName', async () => {
//         const data = park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.Item.forEach((poi: Item) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.originalName === poi.Name['nl-NL']);
//         });
//       });
//       it('parkId', async () => {
//         const data = park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.Item.forEach((poi: Item) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.parkId === 'avonturenpark-hellendoorn');
//         });
//       });
//       it('latitude', async () => {
//         const data = park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.Item.forEach((poi: Item) => {
//           const processed = park.processAttractionPOI(poi);
//           const latlonSplit = poi.Location.split(',');
//           assert(
//             processed.latitude === parseFloat(latlonSplit[0]),
//             'Processed latitude does not match expected return.',
//           );
//         });
//       });
//       it('latitude', async () => {
//         const data = park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.Item.forEach((poi: Item) => {
//           const processed = park.processAttractionPOI(poi);
//           const latlonSplit = poi.Location.split(',');
//           assert(
//             processed.longitude === parseFloat(latlonSplit[1]),
//             'Processed longitude does not match expected return.',
//           );
//         });
//       });
//     });
//     context('Translation', () => {
//       it('language', async () => {
//         const data = park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.Item.forEach((poi: Item) => {
//           const processed = park.processTranslation(poi, 'nl');
//           assert(processed.language === 'nl');
//         });
//       });
//       it('name', async () => {
//         const data = park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.Item.forEach((poi: Item) => {
//           const processed = park.processTranslation(poi, 'nl');
//           assert(processed.name === poi.Name['nl-NL']);
//         });
//       });
//     });
//   });
// });
