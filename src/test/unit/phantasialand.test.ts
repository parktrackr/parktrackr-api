// import { assert } from 'chai';
// import api from '../../lib';
// import * as connections from '../../lib/parks/connections';
// import { POI } from '../../lib/parks/phantasialand/phantasialand.types';

// const park = new api.parks.Phantasialand(connections.Phantasialand());

// describe('Phantasialand', () => {
//   context('Fetching', () => {
//     it('German POI', async () => {
//       const data = await park.fetchPOIs();
//       if (!data) {
//         throw new Error('');
//       }
//       //onsole.log(data[0]);
//     });
//   });
//   context('Processing', () => {
//     context('Attraction POI', () => {
//       it('id', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.id === `${poi.poiNumber}`);
//         });
//       });
//       it('originalName', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.originalName === poi.title.de);
//         });
//       });
//       it('parkId', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.parkId === 'phantasialand');
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.latitude === poi.entrance.world.lat, 'Processed latitude does not match expected return.');
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.longitude === poi.entrance.world.lng, 'Processed longitude does not match expected return.');
//         });
//       });
//     });
//     context('Translation', () => {
//       it('language', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.forEach((poi: POI) => {
//           const processed = park.processTranslation(poi, 'nl');
//           assert(processed.language === 'nl');
//         });
//       });
//       it('name', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.forEach((poi: POI) => {
//           const processed = park.processTranslation(poi, 'nl');
//           assert(processed.name === poi.title.nl);
//         });
//       });
//     });
//   });
// });
