// import { assert } from 'chai';
// import api from '../../lib';
// import * as connections from '../../lib/parks/connections';
// import { POI } from '../../lib/parks/toverland/toverland.types';

// const park = new api.parks.Toverland(connections.Toverland());

// describe('Toverland', () => {
//   context('Fetching', () => {
//     it('POIS', async () => {
//       const data = await park.fetchPOIs();
//       if (!data) {
//         throw new Error('');
//       }
//       assert(data.length > 0, 'POIS has no length.');
//     });
//   });
//   context('Processing', () => {
//     context('Attraction POI', () => {
//       it('id', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.id === `${poi.id}`);
//         });
//       });
//       it('originalName', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.originalName === poi.name);
//         });
//       });
//       it('parkId', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.parkId === 'toverland');
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.latitude === parseFloat(poi.latitude), 'Processed latitude does not match expected return.');
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }

//         data.forEach((poi: POI) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(
//             processed.longitude === parseFloat(poi.longitude),
//             'Processed longitude does not match expected return.',
//           );
//         });
//       });
//     });
//     context('Translation', () => {
//       it('language', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.forEach((poi: POI) => {
//           const processed = park.processTranslation(poi, 'nl');
//           assert(processed.language === 'nl');
//         });
//       });
//       it('name', async () => {
//         const data = await park.fetchPOIs();
//         if (data === null) {
//           throw new Error('');
//         }
//         data.forEach((poi: POI) => {
//           const processed = park.processTranslation(poi, 'nl');
//           assert(processed.name === poi.name);
//         });
//       });
//     });
//   });
// });
