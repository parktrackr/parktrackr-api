// import { assert } from 'chai';
// import api from '../../lib';
// import * as connections from '../../lib/parks/connections';
// import { Entertainment } from '../../lib/parks/walibi/walibi.types';

// const park = new api.parks.WalibiHolland(connections.WalibiHolland());

// describe('Walibi', () => {
//   context('Fetching', () => {
//     it('POIS', async () => {
//       const data = await park.fetchPOI('nl');
//       if (!data) {
//         throw new Error('');
//       }
//       assert(data.entertainment.length > 0);
//     });
//   });
//   context('Processing', () => {
//     context('Attraction POI', () => {
//       it('id', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         data.entertainment.forEach((poi: Entertainment) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.id === poi.uuid);
//         });
//       });
//       it('originalName', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         data.entertainment.forEach((poi: Entertainment) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.originalName === poi.title);
//         });
//       });
//       it('parkId', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }

//         data.entertainment.forEach((poi: Entertainment) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(processed.parkId === 'walibi-holland');
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }

//         data.entertainment.forEach((poi: Entertainment) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(
//             processed.latitude === parseFloat(poi.location.lat),
//             'Processed latitude does not match expected return.',
//           );
//         });
//       });
//       it('latitude', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }

//         data.entertainment.forEach((poi: Entertainment) => {
//           const processed = park.processAttractionPOI(poi);
//           assert(
//             processed.longitude === parseFloat(poi.location.lon),
//             'Processed longitude does not match expected return.',
//           );
//         });
//       });
//     });
//     context('Translation', () => {
//       it('language', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         data.entertainment.forEach((poi: Entertainment) => {
//           const processed = park.processTranslation(poi, 'nl');
//           assert(processed.language === 'nl');
//         });
//       });
//       it('name', async () => {
//         const data = await park.fetchPOI('nl');
//         if (data === null) {
//           throw new Error('');
//         }
//         data.entertainment.forEach((poi: Entertainment) => {
//           const processed = park.processTranslation(poi, 'nl');
//           assert(processed.name === poi.title);
//         });
//       });
//     });
//   });
// });
